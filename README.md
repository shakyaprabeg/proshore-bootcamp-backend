# README.md

- Clone the project
```sh
cd /path/to/project/dir
git clone git@bitbucket.org:shakyaprabeg/proshore-bootcamp-backend.git
```
- Install composer packages
```sh
composer install
```
- Run migration with seeds
```sh
php artisan migrate --seed
``` 
- Run local server
```sh
php artisan serve
```
